const mongoose = require('mongoose');
const RoleSchema = new mongoose.Schema({
  contest: Boolean,
  blog: Boolean,
  category: Boolean,
  exam: Boolean,
  teacher: Boolean,
  testimonial: Boolean,
  register: Boolean,
  jobs: Boolean,
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }
},
  { timestamps: true }
);

module.exports = mongoose.model('Role', RoleSchema);