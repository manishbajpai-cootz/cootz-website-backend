let mongoose = require('mongoose');

let categorySchema = new mongoose.Schema({
   title: String,
   shortDesc: String,
   thumbnail: String,
   publisher: String,
   createdBy: String,
   updatedBy: String
}, 
{timestamps:true}
)

let Category = mongoose.model('Category',categorySchema);

module.exports = Category;