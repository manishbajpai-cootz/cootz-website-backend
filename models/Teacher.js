let mongoose = require('mongoose');
let teacherSchema = new mongoose.Schema({
    name: String,
    image: String,
    coaching: String,
    phone: Array,
    email: String,
    comments: Array,
    rating: Number,
    city: String,
    state: String,
    country: String,
    subject: String,
    createdBy: String,
    updatedBy: String
},
    { timestamps: true }
)
let Teacher = mongoose.model('Teacher', teacherSchema);

module.exports = Teacher;