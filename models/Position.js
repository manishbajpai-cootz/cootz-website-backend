let mongoose = require('mongoose');

let positionSchema = new mongoose.Schema({
    title: String,
    minimumQualifications: String,
    preferredQualifications: String,
    responsibilities: String,
    applicants: Array,
    createdBy: String,
    updatedBy: String
},
    { timestamps: true }
)

let Position = mongoose.model('Position', positionSchema);

module.exports = Position;