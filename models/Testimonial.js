let mongoose = require('mongoose');
let testimonialSchema = new mongoose.Schema({
    mediaUrl: String,
    mediaType: String,
    name: String,
    body: String,
    createdBy: String,
    updatedBy: Array
},
    { timestamps: true }
)
let Testimonial = mongoose.model('Testimonial', testimonialSchema);

module.exports = Testimonial;