let mongoose = require('mongoose');

let subjectSchema = new mongoose.Schema({
   name: String,
   description: String,
   createdBy: String,
   updatedBy: String
}, 
{timestamps:true}
)

let Subject = mongoose.model('Subject',subjectSchema);

module.exports = Subject;