let mongoose = require('mongoose');

let blogSchema = new mongoose.Schema({
   title: String,
   shortDesc: String,
   body: String,
   thumbnail: String,
   keywords: String,
   Publisher:Object,
   slug:String,
   BookmarkedbyUser:[],
   totalReadTime:Number,
   Comments:[{}],
   postedOn:String,
   updatedOn:String,
   tags:[],
   Liked:Number,
   Bookmark:Number,
   status: {
      type: String,
      enum: ['draft','publish']
   },
   createdBy: String,
   updatedBy: String
}, 
{timestamps:true}
)

let Blog = mongoose.model('Blog',blogSchema);

module.exports = Blog;