let mongoose = require('mongoose');

let examSchema = new mongoose.Schema({
    title: String,
    thumbnail: String,
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    details: String,
    slug:String,
    keywords: String,
    createdBy: String,
    updatedBy: String
},
    { timestamps: true }
)

let Exam = mongoose.model('Exam', examSchema);

module.exports = Exam;