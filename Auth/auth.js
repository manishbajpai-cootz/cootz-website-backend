let jwt = require('jsonwebtoken')

exports.auth = async (req,res,next) => {
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')

    token = req.headers.authorization.split(' ')[1];

    if(!token) return res.send({error:"Not authorized to access this route"})

    if(token) {
        try {
        let decoded = await jwt.verify(token, "cootz_secret")
        req.user = decoded
        next()
        } catch(err) {
            res.send(err)
        }
    }
}