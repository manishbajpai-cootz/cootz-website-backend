let express = require('express')
const asyncHandler = require('express-async-handler');
let router = express.Router();
let Blog = require('../models/Blog');
const Category = require('../models/Category');
const Exam = require('../models/Exam');
const Teacher = require('../models/Teacher');
var bcrypt = require('bcryptjs');
const User = require('../models/User');
let jwt = require("jsonwebtoken");
const { auth } = require('../Auth/auth');
const Testimonial = require('../models/Testimonial');
const Role = require('../models/Role');
const Subject = require('../models/Subject');
const Position = require('../models/Position');




// for blogs -------------------------
router.post('/addBlog', async (req, res, next) => {
    let data = await Blog.create({
        title: req.body.title,
        shortDesc: req.body.shortDesc,
        body: req.body.body,
        thumbnail: req.body.thumbnail,
        keywords: req.body.keywords,
        slug: req.body.slug,
        status: req.body.status,
        createdBy: req.body.createdBy
    })
    res.status(200).send(data)
})

router.get('/getAllBlogs', async (req, res, next) => {
    let data = await Blog.find();
    res.status(200).send(data)
})

router.get('/getBlogById/:id', async (req, res, next) => {
    let data = await Blog.find({ _id: req.params.id })
    res.status(200).send(data)
})

router.get('/getBlogBySlug/:slug', async (req, res, next) => {
    let data = await Blog.find({ slug: req.params.slug })
    if(data.length !== 0){
        let arr = []
        let words = data[0].keywords.split(',')
        let randomKeyword = words[Math.floor(Math.random() * words.length - 1) + 1]  
        let similar = await Blog.find({keywords: {$regex: randomKeyword}}).limit(8)
        res.status(200).send({blog:data[0],similar:similar})
    } else {
        res.status(204).send({error: 'No documents found'})
    }
    
})

router.put('/editBlog/:id', async (req, res, next) => {
    let data = await Blog.findByIdAndUpdate(req.params.id, {
        title: req.body.title,
        shortDesc: req.body.shortDesc,
        body: req.body.body,
        thumbnail: req.body.thumbnail,
        keywords: req.body.keywords,
        status: req.body.status,
        slug: req.body.slug,
        $push: {
            updatedBy: req.body.updatedBy
        }
    })
    res.status(200).send(data)
})

router.delete('/deleteBlog/:id', async (req, res, next) => {
    let data = await Blog.findByIdAndDelete(req.params.id)
    res.status(200).send(data)
})


// for category ---------------------
router.post('/addCategory', async (req, res, next) => {
    let data = await Category.create({
        title: req.body.title,
        shortDesc: req.body.shortDesc,
        thumbnail: req.body.thumbnail,
        createdBy: req.body.createdBy
    })
    res.status(200).send(data)
})

router.get('/getAllCategories', async (req, res, next) => {
    let data = await Category.find()
    res.status(200).send(data)
})

router.get('/getCategoryById/:id', async (req, res, next) => {
    let data = await Category.find({ _id: req.params.id })
    res.status(200).send(data)
})

router.put('/editCategory/:id', async (req, res, next) => {
    let data = await Category.findByIdAndUpdate(req.params.id, {
        title: req.body.title,
        shortDesc: req.body.shortDesc,
        thumbnail: req.body.thumbnail,
        $push: {
            updatedBy: req.body.updatedBy
        }
    })
    res.status(200).send(data)
})

router.delete('/deleteCategory/:id', async (req, res, next) => {
    let data = await Category.findByIdAndDelete(req.params.id)
    res.status(200).send(data)
})

// for exams ----------------------------
router.post('/addExam', async (req, res, next) => {
    let data = await Exam.create({
        title: req.body.title,
        thumbnail: req.body.thumbnail,
        category: req.body.category,
        details: req.body.details,
        keywords: req.body.keywords,
        slug: req.body.slug,
        createdBy: req.body.createdBy
    })
    res.status(200).send(data)
})

router.get('/getAllExams', async (req, res, next) => {
    let data = await Exam.find().populate('category')
    res.status(200).send(data)
})

router.delete('/deleteExam/:id', async (req, res, next) => {
    let data = await Exam.findByIdAndDelete(req.params.id)
    res.status(200).send(data)
})

router.get('/getExamById/:id', async (req, res, next) => {
    let data = await Exam.find({ _id: req.params.id }).populate('category')
    res.status(200).send(data)
})

router.get('/getExamBySlug/:slug', async (req, res, next) => {
    let data = await Exam.find({ slug: req.params.slug}).populate('category')
    res.status(200).send(data)
})

router.put('/editExam/:id', async (req, res, next) => {
    let data = await Exam.findByIdAndUpdate(req.params.id, {
        title: req.body.title,
        thumbnail: req.body.thumbnail,
        category: req.body.category,
        details: req.body.details,
        keywords: req.body.keywords,
        slug: req.body.slug,
        $push: {
            updatedBy: req.body.updatedBy
        }
    })
    res.status(200).send(data)
})


// for subjects -------------------------
router.post('/addSubject', async (req, res, next) => {
    let data = await Subject.create({
        name: req.body.name,
        description: req.body.description,
        createdBy: req.body.createdBy
    })
    res.status(200).send(data)
})

router.get('/getAllSubjects', async (req, res, next) => {
    let data = await Subject.find()
    res.status(200).send(data)
})

// for teacher ---------------------------
router.post('/addTeacher', async (req, res, next) => {
    let phone = await Teacher.findOne({ phone: req.body.phone })
    let email = await Teacher.findOne({ email: req.body.email })

    if (phone) return res.status(201).send({ error: "This phone number already exists" })
    if (email) return res.status(201).send({ error: "This email already exists" })

    let data = await Teacher.create({
        name: req.body.name,
        image: req.body.image,
        coaching: req.body.coaching,
        phone: req.body.phones,
        email: req.body.email,
        rating: req.body.rating,
        country: req.body.country,
        state: req.body.state,
        city: req.body.city,
        subject: req.body.subject,
        createdBy: req.body.createdBy
    })
    res.status(200).send(data)
})

router.get('/getAllTeachers', async (req, res, next) => {
    let data = await Teacher.find()
    res.status(200).send(data)
})

router.get('/getTeacherById/:id', async (req, res, next) => {
    let data = await Teacher.find({ _id: req.params.id })
    res.status(200).send(data)
})

router.get('/getTeachersByCreator/:email', async (req, res, next) => {
    let data = await Teacher.find({ createdBy: req.params.email })
    res.status(200).send(data)
})

router.put('/editTeacher/:id', async (req, res, next) => {
    let data = await Teacher.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        image: req.body.image,
        coaching: req.body.coaching,
        phone: req.body.phones,
        email: req.body.email,
        rating: req.body.rating,
        country: req.body.country,
        state: req.body.state,
        city: req.body.city,
        subject: req.body.subject,
        $push: {
            updatedBy: req.body.updatedBy
        }
    })
    res.status(200).send(data)
})

router.delete('/deleteTeacher/:id', async (req, res, next) => {
    let data = await Teacher.findByIdAndDelete(req.params.id)
    res.status(200).send(data)
})


// for testimonial ---------------------------
router.post('/addTestimonial', async (req, res, next) => {
    let data = await Testimonial.create({
        mediaUrl: req.body.mediaUrl,
        mediaType: req.body.mediaType,
        name: req.body.name,
        body: req.body.body,
        createdBy: req.body.createdBy
    })
    res.status(200).send(data)
})

router.get('/getAllTestimonials', async (req, res, next) => {
    let data = await Testimonial.find()
    res.status(200).send(data)
})

router.delete('/deleteTestimonial/:id', async (req, res, next) => {
    let data = await Testimonial.findByIdAndDelete(req.params.id)
    res.status(200).send(data)
})

router.get('/getTestimonialById/:id', async (req, res, next) => {
    let data = await Testimonial.find({ _id: req.params.id })
    res.status(200).send(data)
})

router.put('/editTestimonial/:id', async (req, res, next) => {
    let data = await Testimonial.findByIdAndUpdate(req.params.id, {
        mediaUrl: req.body.mediaUrl,
        mediaType: req.body.mediaType,
        name: req.body.name,
        body: req.body.body,
        $push: {
            updatedBy: req.body.updatedBy
        }
    })
    res.status(200).send(data)
})

// for user --------------------------
router.post('/register', async (req, res, next) => {

    let user = await User.findOne({ email: req.body.email })
    if (user) return res.send({ error: 'User already exists' })

    let hashedPassword = bcrypt.hashSync(req.body.password, 8);
    try {
        let data = await User.create({
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword,
        })
        let role = await Role.create({
            contest: req.body.contest,
            blog: req.body.blog,
            category: req.body.category,
            exam: req.body.exam,
            teacher: req.body.teacher,
            testimonial: req.body.testimonial,
            register: req.body.register,
            jobs: req.body.jobs,
            userId: data._id
        })
        res.status(200).send(data)
    } catch (err) {
        res.send(err)
    }
})

router.post('/login', async (req, res, next) => {
    let user = await User.findOne({ email: req.body.email })

    if (!user) {
        return res.send({ error: "Cannot find anyone by this email" })
    }

    let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) return res.send({ error: 'Password is incorrect' })

    if (passwordIsValid) {

        // Authenticated:
        let payload = {
            id: user._id,
            name: user.name,
            email: user.email,
            role: user.role
        }
        let token = jwt.sign(payload, 'cootz_secret', {
            expiresIn: '365d' // expires in 24 hours
        });
        res.status(200).send({
            auth: true,
            role: user.role,
            token
        })
    }

})

// for roles ---------------------------

router.get('/getAllRoles', async (req, res, next) => {
    try {
        let data = await Role.find().populate('userId')
        res.status(200).send(data)
    } catch (err) {
        res.send(err)
    }
})

router.put('/updateRole/:id', async (req, res, next) => {
    try {
        let data = await Role.findByIdAndUpdate(req.params.id,{
            contest: req.body.contest,
            blog: req.body.blog,
            category: req.body.category,
            exam: req.body.exam,
            teacher: req.body.teacher,
            testimonial: req.body.testimonial,
            register: req.body.register,
            jobs: req.body.jobs
        })
        
            res.status(200).send(data)
    } catch (err) {
        res.send(err)
    }
})

router.get('/getRoleById/:id', async (req, res, next) => {
    try {
        let data = await Role.find({ userId: req.params.id })
        res.status(200).send(data)
    } catch (err) {
        res.send(err)
    }
})




// for positionssss -----------

router.get('/getAllPositions', async (req, res, next) => {
    try {
        let data = await Position.find()
        res.status(200).send(data)
    } catch (err) {
        res.send(err)
    }
})

router.post('/addPosition', async (req, res, next) => {
    let data = await Position.create({
        title: req.body.name,
        minimumQualifications: req.body.minimumQualifications,
        preferredQualifications: req.body.preferredQualifications,
        responsibilities: req.body.responsibilities,
        createdBy: req.body.createdBy
    })
    res.status(200).send(data)
})

router.delete('/deletePosition/:id', async (req, res, next) => {
    let data = await Position.findByIdAndDelete(req.params.id)
    res.status(200).send(data)
})

router.get('/getPositionById/:id', async (req, res, next) => {
    let data = await Position.find({ _id: req.params.id })
    res.status(200).send(data)
})

router.put('/editPosition/:id', async (req, res, next) => {
    let data = await Position.findByIdAndUpdate(req.params.id, {
        title: req.body.name,
        minimumQualifications: req.body.minimumQualifications,
        preferredQualifications: req.body.preferredQualifications,
        responsibilities: req.body.responsibilities,
        $push: {
            applicants: req.body.resume
        }
    })
    res.status(200).send(data)
})


module.exports = router

